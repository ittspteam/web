package pl.ittspteam.ittsp.services;

import pl.ittspteam.ittsp.api.domain.GoogleAPIResponse;

public interface MatrixService {
    String convertGoogleApiResponseToMatrixFile(GoogleAPIResponse googleAPIResponse);
}
