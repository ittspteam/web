package pl.ittspteam.ittsp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ittspteam.ittsp.api.domain.GoogleAPIResponse;
import pl.ittspteam.ittsp.api.domain.OptimalTrip;
import pl.ittspteam.ittsp.entity.RequestMappingData;
import pl.ittspteam.ittsp.repositories.RequestMappingDataRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RequestMappingServiceImpl implements RequestMappingService {

    private RequestMappingDataRepository repository;

    @Autowired
    public RequestMappingServiceImpl(RequestMappingDataRepository repository) {
        this.repository = repository;
    }

    @Override
    public void saveRequestMappingData(GoogleAPIResponse googleAPIResponse, String requestId) {
        List<RequestMappingData> data = new ArrayList<>();
        List<String> originAddresses = googleAPIResponse.getOriginAddresses();

        for (int i = 0; i < originAddresses.size(); i++) {
            RequestMappingData mappingData = new RequestMappingData();
            mappingData.setOriginId(i);
            mappingData.setOriginName(originAddresses.get(i));
            mappingData.setRequestId(requestId);
            data.add(mappingData);
        }
        repository.save(data);
    }

    @Override
    public OptimalTrip convertOriginIdsToOptimalTrip(List<Integer> ids, String requestId) {
        OptimalTrip optimalTrip = new OptimalTrip();
        Map<Integer, String> mappings = repository.findByRequestId(requestId).stream()
                .collect(Collectors.toMap(RequestMappingData::getOriginId, RequestMappingData::getOriginName));
        ids.forEach(id -> optimalTrip.getDestinations().add(mappings.get(id)));
        return optimalTrip;
    }
}
