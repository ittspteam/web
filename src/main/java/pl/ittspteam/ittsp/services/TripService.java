package pl.ittspteam.ittsp.services;

import pl.ittspteam.ittsp.api.domain.OptimalTrip;

import java.util.List;

public interface TripService {
    OptimalTrip findOptimalTrip(List<String> origins);
}
