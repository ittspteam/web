package pl.ittspteam.ittsp.services;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ASTPSServiceImpl implements ASTPSService {

    private static final Logger LOG = LoggerFactory.getLogger(ASTPSService.class);
    private static final String SOLUTION = "Solution: ";

    private String serviceName;
    private String serviceDirectory;

    @Autowired
    public ASTPSServiceImpl(@Value("${astps.service.name}") String serviceName,
                            @Value("${astps.service.directory}") String serviceDirectory) {
        this.serviceName = serviceName;
        this.serviceDirectory = serviceDirectory;
    }

    @Override
    public List<Integer> executeASTPSService(String inputFileName) {
        List<Integer> originIds = new ArrayList<>();
        String servicePath = this.createRelativeServicePath();
        String inputFilePath = this.createRelativeInputFilePath(inputFileName);

        try {
            LOG.debug("Starting....");
            Process process = new ProcessBuilder(servicePath, inputFilePath).start();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                String line;
                while ((line = br.readLine()) != null) {
                    processOutputLine(originIds, line);
                }
            }
        } catch (IOException ex) {
            LOG.error("ASTPS service IOException: " + ex.getMessage());
        }
        LOG.debug("return: " + originIds.toString());
        return originIds;
    }

    private void processOutputLine(List<Integer> ids, String line) {
        LOG.debug(line);
        if (StringUtils.contains(line, SOLUTION)) {
            String results = StringUtils.substringAfter(line, SOLUTION);
            Arrays.stream(StringUtils.split(results, " "))
                    .map(Integer::valueOf)
                    .forEach(ids::add);
        }
    }

    private String createRelativeServicePath() {
        return this.createRelativePath(serviceName);
    }

    private String createRelativeInputFilePath(String inputFileName) {
        return this.createRelativePath(inputFileName);
    }

    private String createRelativePath(String name) {
        return serviceDirectory + name;
    }
}
