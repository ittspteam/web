package pl.ittspteam.ittsp.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.ittspteam.ittsp.api.domain.GoogleAPIResponse;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GoogleAPIServiceImpl implements GoogleAPIService {

    private RestTemplate restTemplate;
    private String apiUrl;
    private String apiKey;

    @Autowired
    public GoogleAPIServiceImpl(RestTemplate restTemplate, @Value("${api.url}") String apiUrl,
                                @Value("${api.key}") String apiKey) {
        this.restTemplate = restTemplate;
        this.apiUrl = apiUrl;
        this.apiKey = apiKey;
    }

    @Override
    public GoogleAPIResponse retrieveDistanceMatrixResponse(List<String> origins) {
        String originsAsString = this.createQueryParamList(origins);

        UriComponents uriComponents = UriComponentsBuilder
                .fromUriString(apiUrl)
                .queryParam("key", apiKey)
                .queryParam("origins", originsAsString)
                .queryParam("destinations", originsAsString)
                .build(false);

        return restTemplate.getForObject(uriComponents.toUriString(), GoogleAPIResponse.class);
    }

    private String createQueryParamList(List<String> params) {
        List<String> encodedParams = params.stream()
                .map(param -> StringUtils.replace(param, " ", "+"))
                .map(param -> "place_id:" + param)
                .collect(Collectors.toList());
        return StringUtils.join(encodedParams, "|");
    }
}
