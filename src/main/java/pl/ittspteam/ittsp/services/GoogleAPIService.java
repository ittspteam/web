package pl.ittspteam.ittsp.services;

import pl.ittspteam.ittsp.api.domain.GoogleAPIResponse;

import java.util.List;

public interface GoogleAPIService {
    GoogleAPIResponse retrieveDistanceMatrixResponse(List<String> origins);
}
