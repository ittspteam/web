package pl.ittspteam.ittsp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ittspteam.ittsp.api.domain.GoogleAPIResponse;
import pl.ittspteam.ittsp.api.domain.OptimalTrip;

import java.util.List;
import java.util.UUID;

@Service
public class TripServiceImpl implements TripService {

    private GoogleAPIService googleAPIService;
    private MatrixService matrixService;
    private RequestMappingService requestMappingService;
    private ASTPSService astpsService;

    @Autowired
    public TripServiceImpl(GoogleAPIService googleAPIService, ASTPSService astpsService,
                           RequestMappingService requestMappingService, MatrixService matrixService) {
        this.googleAPIService = googleAPIService;
        this.matrixService = matrixService;
        this.requestMappingService = requestMappingService;
        this.astpsService = astpsService;
    }

    @Override
    public OptimalTrip findOptimalTrip(List<String> origins) {
        String requestId = UUID.randomUUID().toString();
        GoogleAPIResponse googleAPIResponse = googleAPIService.retrieveDistanceMatrixResponse(origins);
        requestMappingService.saveRequestMappingData(googleAPIResponse, requestId);
        String inputFileName = matrixService.convertGoogleApiResponseToMatrixFile(googleAPIResponse);
        List<Integer> originIds = astpsService.executeASTPSService(inputFileName);
        return requestMappingService.convertOriginIdsToOptimalTrip(originIds, requestId);
    }
}
