package pl.ittspteam.ittsp.services;

import java.util.List;

public interface ASTPSService {
    List<Integer> executeASTPSService(String inputFileName);
}
