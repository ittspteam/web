package pl.ittspteam.ittsp.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.ittspteam.ittsp.api.domain.GoogleAPIResponse;
import pl.ittspteam.ittsp.api.domain.OriginResult;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
public class MatrixServiceImpl implements MatrixService {

    private static final Logger LOG = LoggerFactory.getLogger(MatrixServiceImpl.class);

    private String serviceDirectory;

    @Autowired
    public MatrixServiceImpl(@Value("${astps.service.directory}") String serviceDirectory) {
        this.serviceDirectory = serviceDirectory;
    }

    @Override
    public String convertGoogleApiResponseToMatrixFile(GoogleAPIResponse googleAPIResponse) {
        Map<Integer, List<Long>> originDistances = createOriginDistancesMap(googleAPIResponse);
        return saveMatrixToFile(originDistances);
    }

    private String saveMatrixToFile(Map<Integer, List<Long>> originDistances) {
        File file = createOutputFile();
        int matrixSize = originDistances.keySet().size();

        try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
            out.write(matrixSize + "\n");

            for (int i = 0; i < matrixSize; i++) {
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j <= i; j++) {
                    List<Long> distances = originDistances.get(j);
                    sb.append(distances.get(i)).append(" ");
                }
                sb.append("\n");
                out.write(sb.toString());
            }

        } catch (IOException ex) {
            LOG.error("IOException " + ex.getMessage());
        }
        LOG.debug("File created in path: " + file.getAbsolutePath());
        return file.getName();
    }

    private Map<Integer, List<Long>> createOriginDistancesMap(GoogleAPIResponse googleAPIResponse) {
        Map<Integer, List<Long>> originToDistances = new TreeMap<>();
        List<OriginResult> origins = googleAPIResponse.getOriginResults();

        for (int i = 0; i < origins.size(); i++) {
            List<Long> distances = new ArrayList<>();
            origins.get(i).getElements().forEach(element -> distances.add(element.getDistance().getValue()));
            originToDistances.put(i, distances);
        }
        return originToDistances;
    }

    private File createOutputFile() {
        return new File(serviceDirectory + "matrix" + System.currentTimeMillis() + ".txt");
    }
}
