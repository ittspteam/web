package pl.ittspteam.ittsp.services;

import pl.ittspteam.ittsp.api.domain.GoogleAPIResponse;
import pl.ittspteam.ittsp.api.domain.OptimalTrip;

import java.util.List;

public interface RequestMappingService {

    void saveRequestMappingData(GoogleAPIResponse googleAPIResponse, String requestId);

    OptimalTrip convertOriginIdsToOptimalTrip(List<Integer> ids, String requestId);
}
