package pl.ittspteam.ittsp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.ittspteam.ittsp.api.domain.OptimalTrip;
import pl.ittspteam.ittsp.services.TripService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/trip")
public class RestApiController {

    private TripService tripService;

    @Autowired
    public RestApiController(TripService tripService) {
        this.tripService = tripService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public OptimalTrip findOptimalTrip(@RequestParam List<String> origins) {
        return tripService.findOptimalTrip(origins);
    }
}
