package pl.ittspteam.ittsp.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @Value("${api.key}")
    private String apiKey;

    @RequestMapping({"/", ""})
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/trip")
    public ModelAndView showTripPage(ModelMap map) {
        map.addAttribute("apiKey", apiKey);
        return new ModelAndView("trip", map);
    }
    @GetMapping({"/aboutus"})
        public String showAboutUsPage() { return "aboutus"; }

    @GetMapping({"/algorithms"})
        public String showAlgorithmsPage() { return "algorithms"; }

    @GetMapping({"/maptest"})
    public ModelAndView showMapTestPage(ModelMap map) {
        map.addAttribute("apiKey", apiKey);
        return new ModelAndView("maptest", map);
    }

}
