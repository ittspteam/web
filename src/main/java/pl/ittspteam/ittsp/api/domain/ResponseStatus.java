package pl.ittspteam.ittsp.api.domain;

public enum ResponseStatus
{
    OK, NOT_FOUND, ZERO_RESULTS, MAX_ROUTE_LENGHT_EXCEDED, OVER_QUERY_LIMIT
}
