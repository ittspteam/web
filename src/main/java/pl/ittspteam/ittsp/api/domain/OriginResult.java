package pl.ittspteam.ittsp.api.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OriginResult
{
    @JsonProperty("elements")
    private List<DestinationResult> elements;

    public List<DestinationResult> getElements() {
        return elements;
    }

    public void setElements(List<DestinationResult> elements) {
        this.elements = elements;
    }
}
