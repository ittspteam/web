package pl.ittspteam.ittsp.api.domain;

import java.util.ArrayList;
import java.util.List;

public class OptimalTrip {
    private List<String> destinations;

    public OptimalTrip() {
        this.destinations = new ArrayList<>();
    }

    public List<String> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<String> destinations) {
        this.destinations = destinations;
    }
}
