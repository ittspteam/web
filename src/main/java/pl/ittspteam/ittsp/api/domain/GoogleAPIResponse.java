package pl.ittspteam.ittsp.api.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GoogleAPIResponse
{
    @JsonProperty("destination_addresses")
    private List<String> destinationAddresses;
    @JsonProperty("origin_addresses")
    private List<String> originAddresses;
    @JsonProperty("rows")
    private List<OriginResult> originResults;
    @JsonProperty("status")
    private ResponseStatus status;

    public List<String> getDestinationAddresses() {
        return destinationAddresses;
    }

    public void setDestinationAddresses(List<String> destinationAddresses) { this.destinationAddresses = destinationAddresses; }

    public List<String> getOriginAddresses() { return originAddresses; }

    public void setOriginAddresses(List<String> originAddresses) {
        this.originAddresses = originAddresses;
    }

    public List<OriginResult> getOriginResults() {
        return originResults;
    }

    public void setOriginResults(List<OriginResult> originResults) {
        this.originResults = originResults;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }
}
