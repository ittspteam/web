package pl.ittspteam.ittsp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ittspteam.ittsp.entity.RequestMappingData;

import java.util.List;

@Repository
public interface RequestMappingDataRepository extends JpaRepository<RequestMappingData, Integer> {

    List<RequestMappingData> findByRequestId(String requestId);
}
