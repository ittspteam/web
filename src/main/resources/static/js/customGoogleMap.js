function initAutocomplete() {
    var input = document.getElementById('pac-input');
    var defaultBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(33.5, -11.0),
        new google.maps.LatLng(72.5, 46.0));

    var autocomplete = new google.maps.places.Autocomplete(input, {bounds: defaultBounds});
    autocomplete.addListener('place_changed', function () {
        var list = $('#result-list');
        var input = $('#pac-input');

        if (list.find("tr").length > 9) {
            $('.alert').show();
        } else {
            var info = autocomplete.getPlace();
            if (!info.address_components) {
                return;
            }
            var addressName = info.adr_address;
            var addressValue = info.place_id;
            var removeBtn = $('<button id="removeBtn" value="Remove" class="btn btn-success button-cus pull-right">Remove</button>');
            var addressNameCol = $('<td>').append(addressName);
            var removeCol = $('<td>').append(removeBtn);
            var element = $('<tr>').append(addressNameCol).append(removeCol).attr('address-value', addressValue);
            list.append(element);

            removeBtn.on('click', function () {
                element.detach();
                var $alertMsg = $('.alert');
                $alertMsg.length && $alertMsg.hide();
            });
        }

        input.blur();
        setTimeout(function () {
            input.val('');
            input.focus();
        }, 10);
    });

    $('#find-road').on('click', function () {
        var resultList = $('#find-road-result');
        resultList.text("");
        var origins = [];
        var list = $('#result-list');
        var elements = list.find('tr');

        if (!elements.length) {
            return;
        }

        for (var i = 0; i < elements.length; i++) {
            var value = $(elements[i]).attr('address-value');
            origins.push(value);
        }

        var params = {origins: origins.join(",")};

        var url = window.location.protocol + '//' + window.location.host + '/api/v1/trip';
        $.ajax({
                url: url,
                data: params,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $('#map').height(400).width(650);
                    var directionsDisplay = new google.maps.DirectionsRenderer;
                    var directionsService = new google.maps.DirectionsService;
                    var waypts = [];
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 5,
                        center: {lat: 50.17, lng: 18.90}
                    });
                    directionsDisplay.setMap(map);
                    var destinations = data.destinations;

                    for (var i = 1; i < destinations.length; i++) {
                        waypts.push({
                            location: destinations[i],
                            stopover: true});

                    }

                    directionsService.route({
                        origin: destinations[0],
                        destination: destinations[0],
                        waypoints: waypts,
                        optimizeWaypoints: true,
                        travelMode: 'DRIVING'
                    }, function(response, status) {
                        if (status === 'OK') {
                            directionsDisplay.setDirections(response);
                        } else {
                            window.alert('Directions request failed due to ' + status);
                        }
                    });
                    elements.detach();
                    var $alertMsg = $('.alert');
                    $alertMsg.length && $alertMsg.hide();
                    },
                    error: function (xhr, status) {
                    resultList.append('Error with response: ' + xhr.responseJSON.message);
                    elements.detach();
                    var $alertMsg = $('.alert');
                    $alertMsg.length && $alertMsg.hide();

                    }
            }
        );

    });
}